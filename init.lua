-- This file simply bootstraps the installation of Lazy.nvim and then calls other files for execution
-- This file doesn't necessarily need to be touched, BE CAUTIOUS editing this file and proceed at your own risk.
local lazypath = vim.env.LAZY or vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not (vim.env.LAZY or (vim.uv or vim.loop).fs_stat(lazypath)) then
  -- stylua: ignore
  vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath })
end
vim.opt.rtp:prepend(lazypath)

-- validate that lazy is available
if not pcall(require, "lazy") then
  -- stylua: ignore
  vim.api.nvim_echo({ { ("Unable to load lazy from: %s\n"):format(lazypath), "ErrorMsg" }, { "Press any key to exit...", "MoreMsg" } }, true, {})
  vim.fn.getchar()
  vim.cmd.quit()
end

-- recognize yara filetype
vim.cmd("autocmd BufNewFile,BufRead *.yar,*.yara setlocal filetype=yara")

require "lazy_setup"
require "polish"

-- = = = MY STUFF = = =

local cmp = require('cmp')

vim.cmd("set list")

-- don't yank on paste
vim.cmd("xnoremap p P")

-- vim.cmd("nnoremap x \"_x")
-- vim.cmd("xnoremap x \"_x")
-- vim.cmd("nnoremap d \"_d")
-- vim.cmd("xnoremap d \"_d")

-- vim.api.nvim_create_user_command('StupidWindowsLineEndingStuff', function ()
--     vim.o.scrolloff = 5
--     vim.opt.fileformats = { "unix", "dos"}
--     vim.opt.binary = true
--     vim.opt.eol = false
-- end, {})

vim.api.nvim_create_user_command('DisableCmp', function ()
    require('cmp').setup.buffer { enabled = false }
end, {})

local HOMEDIR = vim.fn.getenv("HOME")
local SPANISH_DIR = HOMEDIR .. "/proton/drive/primary/clep/spanish"
if vim.fn.getcwd():sub(1, SPANISH_DIR:len()) == SPANISH_DIR then
    vim.cmd('set spell')
    vim.cmd('set scrolloff=10')
end

vim.api.nvim_create_user_command('Fmt', function ()
    local filetype = vim.bo.filetype

    if filetype == "sql" then
        vim.cmd(":%!sql-formatter")
    end
end, {})
