-- You can also add or configure plugins by creating files in this `plugins/` folder
-- Here are some examples:

---@type LazySpec
return {

    -- colorscheme
    { "EdenEast/nightfox.nvim" },

    -- {
    --     "hrsh7th/nvim-cmp",
    --     opts = function(_, opts)
    --         opts.mapping["<Tab>"] = function()
    --             if luasnip.jumpable() then
    --                 luasnip.jump(1)
    --             else
    --                 vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Tab>", true, false, true), "n!", false)
    --             end
    --         end
    --     end,
    -- },

    -- snippets
    {
        "L3MON4D3/LuaSnip",
        config = function(plugin, opts)
            -- include the default astronvim config that calls the setup call
            require("astronvim.plugins.configs.luasnip")(plugin, opts)
            -- load snippets paths
            require("snippets.snippets")
            -- require("luasnip.loaders.from_vscode").lazy_load({
            --   paths = { vim.fn.stdpath("config") .. "/snippets" },
            -- })
        end,
    },

    -- markdown
    {
        "https://github.com/deron-dev/fork-clipboard-image.nvim.git",
        event = "BufRead *.md",
        config = function()
            require("clipboard-image").setup()
            -- require("clipboard-image").setup {
            --   -- Default configuration for all filetype
            --   default = {
            --     img_dir = "assets",
            --     img_name = function() return os.date('%Y-%m-%d-%H-%M-%S') end, -- Example result: "2021-04-13-10-04-18"
            --     affix = "<\n  %s\n>"                                           -- Multi lines affix
            --   },
            --   -- You can create configuration for ceartain filetype by creating another field (markdown, in this case)
            --   -- If you're uncertain what to name your field to, you can run `lua print(vim.bo.filetype)`
            --   -- Missing options from `markdown` field will be replaced by options from `default` field
            --   markdown = {
            --     img_dir = { "src", "assets", "img" }, -- Use table for nested dir (New feature form PR #20)
            --     img_dir_txt = "/assets/img",
            --     img_handler = function(img)           -- New feature from PR #22
            --       local script = string.format('./image_compressor.sh "%s"', img.path)
            --       os.execute(script)
            --     end,
            --   }
            -- }
        end,
    },
    {
        "tadmccorkle/markdown.nvim",
        ft = "markdown", -- or 'event = "VeryLazy"'
        opts = {
            -- configuration here or empty for defaults
        },
    },
    { "jannis-baum/vivify.vim" },

    -- rust
    {
        'saecki/crates.nvim',
        event = { "BufRead Cargo.toml" },
        tag = 'stable',
        config = function()
            require('crates').setup()
        end,
    },

    -- javascript
    {
        "danymat/neogen",
        config = true,
        -- Uncomment next line if you want to follow only stable versions
        -- version = "*"
    },

    -- yara syntax
    { "s3rvac/vim-syntax-yara" },

    -- == Examples of Adding Plugins ==

    -- "andweeb/presence.nvim",
    -- {
    --   "ray-x/lsp_signature.nvim",
    --   event = "BufRead",
    --   config = function() require("lsp_signature").setup() end,
    -- },

    -- == Examples of Overriding Plugins ==

    -- customize alpha options
    {
        "goolord/alpha-nvim",
        opts = function(_, opts)
            -- customize the dashboard header
            opts.section.header.val = {
                -- " █████  ███████ ████████ ██████   ██████",
                -- "██   ██ ██         ██    ██   ██ ██    ██",
                -- "███████ ███████    ██    ██████  ██    ██",
                -- "██   ██      ██    ██    ██   ██ ██    ██",
                -- "██   ██ ███████    ██    ██   ██  ██████",
                " ",
                "███    ██ ██    ██ ██ ███    ███",
                "████   ██ ██    ██ ██ ████  ████",
                "██ ██  ██ ██    ██ ██ ██ ████ ██",
                "██  ██ ██  ██  ██  ██ ██  ██  ██",
                "██   ████   ████   ██ ██      ██",
                "                                ",
                "       textual liberation       ",
            }
            return opts
        end,
    },

    -- You can disable default plugins as follows:
    -- { "max397574/better-escape.nvim", enabled = false },

    -- You can also easily customize additional setup of plugins that is outside of the plugin's setup call
    -- {
    --   "L3MON4D3/LuaSnip",
    --   config = function(plugin, opts)
    --     require "astronvim.plugins.configs.luasnip"(plugin, opts) -- include the default astronvim config that calls the setup call
    --     -- add more custom luasnip configuration such as filetype extend or custom snippets
    --     local luasnip = require "luasnip"
    --     luasnip.filetype_extend("javascript", { "javascriptreact" })
    --   end,
    -- },

    -- {
    --   "windwp/nvim-autopairs",
    --   config = function(plugin, opts)
    --     require "astronvim.plugins.configs.nvim-autopairs"(plugin, opts) -- include the default astronvim config that calls the setup call
    --     -- add more custom autopairs configuration such as custom rules
    --     local npairs = require "nvim-autopairs"
    --     local Rule = require "nvim-autopairs.rule"
    --     local cond = require "nvim-autopairs.conds"
    --     npairs.add_rules(
    --       {
    --         Rule("$", "$", { "tex", "latex" })
    --           -- don't add a pair if the next character is %
    --           :with_pair(cond.not_after_regex "%%")
    --           -- don't add a pair if  the previous character is xxx
    --           :with_pair(
    --             cond.not_before_regex("xxx", 3)
    --           )
    --           -- don't move right when repeat character
    --           :with_move(cond.none())
    --           -- don't delete if the next character is xx
    --           :with_del(cond.not_after_regex "xx")
    --           -- disable adding a newline when you press <cr>
    --           :with_cr(cond.none()),
    --       },
    --       -- disable for .vim files, but it work for another filetypes
    --       Rule("a", "a", "-vim")
    --     )
    --   end,
    -- },
}
