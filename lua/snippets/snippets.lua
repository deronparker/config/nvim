local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local l = extras.lambda
local rep = extras.rep
local p = extras.partial
local m = extras.match
local n = extras.nonempty
local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local conds = require("luasnip.extras.expand_conditions")
local postfix = require("luasnip.extras.postfix").postfix
local types = require("luasnip.util.types")
local parse = require("luasnip.util.parser").parse_snippet
local ms = ls.multi_snippet
local k = require("luasnip.nodes.key_indexer").new_key

local function spanish_snippets()
    local HOMEDIR = vim.fn.getenv("HOME")
    local SPANISH_DIR = HOMEDIR .. "/proton/drive/primary/clep/spanish"
    if vim.fn.getcwd():sub(1, SPANISH_DIR:len()) == SPANISH_DIR then
        return {
            s("??", {t"¿", i(1), t"?", i(0)}),
            s("'a", {t"á"}),
            s("'e", {t"é"}),
            s("'i", {t"í"}),
            s("'o", {t"ó"}),
            s("'u", {t"ú"}),
            s("n?", {t"ñ"}),
            s("card", {t"# ", i(1), t"---", i(0)})
        }
    else
        return nil
    end
end

return {
    ls.add_snippets("php", {
        s("php", {
            t"<?php ", i(1), t" ?>"
            }
        ),
        s("phpi", {
            t"?> ", i(1), t" <?php"
            }
        ),
    }),
    ls.add_snippets("markdown", {
        s(";td", t"- [ ] "),
    }),
    ls.add_snippets("markdown", spanish_snippets()),
    ls.add_snippets("go", {
        s(";ife", {
            t({"if err != nil {" , ""}),
            t("\tlog.Fatal(err)"), i(1),
            t({"","}"})
            }
        ),
        s(";iferr", {
            t({"if err != nil {" , ""}),
            t("\t"), i(1),
            t({"","}"})
            }
        ),
    }),
    ls.add_snippets("tex", {
        s(";ms", {
            t({"\\begin{myspan}", ""}),
            i(1),
            t({"","\\end{myspan}"})
            }
        ),
        s(";align", {
            t({"\\begin{align*}", ""}),
            i(1),
            t({"","\\end{align*}"})
            }
        ),
        s(";gather", {
            t({"\\begin{gather*}", ""}),
            i(1),
            t({"","\\end{gather*}"})
            }
        ),
    }),
    ls.add_snippets("rust", {
        s(";tests", {
            t({"#[cfg(test)]", ""}),
            t({"mod tests {", ""}),
            i(0),
            t({"", "}"})
        }),
        s("ttest", {
            t({"#[tokio::test]", ""}),
            t"async fn ", i(0), t({"() {"}),
            t({"", "}"}),
        })
    }),
    ls.add_snippets("javascript", {
        --jsdoc comment
        s(";jd", { t'/** ', i(1), t' */', i(0) } ),
    }),
}
